#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int main()
{
    WINDOW *ablak;
    ablak = initscr();

    int x = 0, y = 0;  //innen indul 
    int xnov = 1, ynov = 1;   //ennyit lép
    int mx, my;

    for (;;) {
        getmaxyx (ablak,mx, my); //a terminál mérete

        mvprintw (y, x, ":)");

        refresh ();
        usleep (100000);    //gyorsaság microsec (1mil=1sec)
        clear();    //törli maga után az előzőt

        x = x + xnov;
        y = y + ynov;

        if (x>=my-1) {
            xnov = xnov * -1;
        }
        if (x<=0) {
            xnov = xnov * -1;
        }
        if (y<=0) {
            ynov = ynov * -1;
        }
        if (y>=mx-1) {
            ynov = ynov * -1;
        }

    }
    return 0;
}