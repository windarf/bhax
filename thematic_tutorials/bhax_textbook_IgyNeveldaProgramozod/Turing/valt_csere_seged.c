#include <stdio.h>

int main()
{
    int a = 11, b = 19, c;
	
	printf("a= %d\nb= %d\n",a,b);

    c = a;
    a = b;
    b = c;

    printf("a= %d\nb= %d\n",a,b);
    
    return 0;
}